package com.dbi.progressive.registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Configuring class which is used to bootstrap
 *  and launch a Spring application 
 * 
 * @author pramanda
 *
 */
@SpringBootApplication
public class ProgressiveRegistrationApplication extends SpringBootServletInitializer {
 
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ProgressiveRegistrationApplication.class);
    }


	public static void main(String[] args) {
		SpringApplication.run(ProgressiveRegistrationApplication.class, args);
	}
	
}