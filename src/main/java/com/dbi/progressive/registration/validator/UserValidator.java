package com.dbi.progressive.registration.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.dbi.progressive.registration.model.User;


/**
 * Validator for user registration request
 * 
 * @author pramanda
 *
 */
@Component
public class UserValidator implements Validator{
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user = (User)target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"firstName", "Firstname cannot be empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"lastName", "lastName cannot be empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"email", "email cannot be empty");
		
		if(user != null && !StringUtils.isEmpty(user.getEmail())) {
			Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(user.getEmail());
			if(!matcher.matches())
				errors.rejectValue("email","Invalid Email format");
		}
	}

}
