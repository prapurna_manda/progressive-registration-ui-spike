
package com.dbi.progressive.registration.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.dbi.progressive.registration.configuration.TilesConfig;
import com.dbi.progressive.registration.model.User;
import com.dbi.progressive.registration.validator.UserValidator;

/**
 * 
 * @author Prapurna 
 *
 */
@Controller
public class RegistrationController {

	private Logger log = LoggerFactory.getLogger(TilesConfig.class);
	
	@Autowired
	UserValidator userValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(userValidator);
	}

	@PostMapping("/registration")
	public ModelAndView registration(  @ModelAttribute("user") @Validated User user, BindingResult result) {
		log.info("[RegistrationController.registration: In Registration]");
		ModelAndView model = new ModelAndView("success");
		
		if(result.hasErrors()) {
			log.debug("Request has invalid input params");
			 model = new ModelAndView("error");
			 model.getModelMap().put("errors" , result.getFieldErrors());
		}else {
			log.info("Registration for user: " + user.getEmail());
			model.getModelMap().put("name", user.getFirstName());
		}

		return model;
	}

	@GetMapping("/")
	public ModelAndView home(@ModelAttribute("user")User user) {
		return new ModelAndView("registration");
	}

	@GetMapping("/error")
	public ModelAndView errorView() {
		return new ModelAndView("error");
	}

}
