package com.dbi.progressive.registration.tags;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.dbi.progressive.registration.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Custom tag implementation for REST calls from JSP's
 * 
 * @author pramanda
 *
 */
public class DBIRestTag extends TagSupport{

	private static final long serialVersionUID = 6769519252223683698L;
	private RestTemplate restTemplate;
	private String	uri;
	private String	method;
	private String var;

	public DBIRestTag() {
		this.restTemplate = new RestTemplateBuilder().build();
	}

	@Override
	public int doEndTag() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);

		// Uncomment this to make it work with actual rest service
		//ResponseEntity<User> response = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>("parameters", headers), User.class);

		InputStream inputStream;
		User user = new User();
		try {
			inputStream = new ClassPathResource("response.json").getInputStream();

			String result = new BufferedReader(new InputStreamReader(inputStream)).lines()
					.collect(Collectors.joining("\n"));
			
			user = new ObjectMapper().readValue(result, User.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// This data is available in Pagecontext and can be used to rendered required details
		super.pageContext.setAttribute(var, user);

		// Uncomment this to make it work with actual rest service
		//super.pageContext.setAttribute(var, response.getBody());

		return 1;
	}


	public RestTemplate getRestTemplate() {
		return restTemplate;
	}
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getVar() {
		return var;
	}
	public void setVar(String var) {
		this.var = var;
	}


}
