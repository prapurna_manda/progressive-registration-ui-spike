<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="dbi" uri="../dbiTagDescriptor.tld"%>
<%@ page import="com.dbi.progressive.registration.model.User"%>

<h4>Register</h4>

<form:form action="/registration" method="post" modelAttribute="user">
	<p>
		<label>Firstname</label>
		<form:input path="firstName" />
		<form:errors path="firstName" cssClass="error" />
	</p>
	<p>
		<label>Lastname</label>
		<form:input path="lastName" />
		<form:errors path="lastName" cssClass="error" />
	</p>
	<p>
		<label>Email</label>
		<form:input path="email" />
		<form:errors path="email" cssClass="error" />
	</p>
	<input type="SUBMIT" value="Submit" />
	
</form:form>

<br />
<dbi:HelloWorld />
<br />
<!-- JSP custom tag for Rest call -->
<dbi:rest uri="GET_DATA"
	method="GET" var="user" />

<p> Rest Call for existing user </p>

<%
    User user = (User) pageContext.getAttribute("user");
    
    out.println("Welcome User:" +user.getFirstName() + user.getLastName() + user.getEmail());
%>

