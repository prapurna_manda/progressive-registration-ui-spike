<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h3>Error occurred , Please try again later!</h3>

<c:if test="${not empty errors}">
	<c:forEach var="error" items="${errors}">
		<c:out value="${error.field}">
		</c:out> :
		<c:out value="${error.codes[3]}">
		</c:out> <br/>
	</c:forEach>
</c:if>	