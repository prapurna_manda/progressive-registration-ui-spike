package com.dbi.progressive.registration.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.base.MockitoException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BindingResult;

import com.dbi.progressive.registration.model.User;
import com.dbi.progressive.registration.validator.UserValidator;


/**
 * Test suite for Registration Controller
 * 
 * @author pramanda
 *
 */
@RunWith(SpringRunner.class)
public class RegistrationControllerTest {

	@InjectMocks
	RegistrationController registrationController;

	@Mock
	UserValidator userValidator;

	@Mock
	private BindingResult bindingResult;

	@Before
	public void setUp() throws MockitoException {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRegistration() {
		Mockito.when(bindingResult.hasErrors()).thenReturn(false);
		registrationController.registration(getMockUser(), bindingResult);
	}

	public User getMockUser() {
		User user = new User();

		user.setFirstName("John");
		user.setFirstName("Doe");
		user.setFirstName("John.Doe@yopmail.com");

		return user;
	}
}
