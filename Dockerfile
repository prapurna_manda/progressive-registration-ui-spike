FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/Progressive-Registration-0.0.1-SNAPSHOT.war target/progreg-poc-app.war
EXPOSE 8080
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=local","target/progreg-poc-app.war"]